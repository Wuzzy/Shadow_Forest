# Shadow Forest

The forest has been overrun by the Shadows and is cursed with eternal darkness.
You, the Wizard of Light, are the final hope.
Find and destroy the source of darkness and bring back the light!

A game by Wuzzy made for the 2023 Minetest Game Jam.

This is version 1.1.2.

Historical note: The version submitted for the game jam was 1.0.1.

## System requirements

You need Minetest 5.8.0 or later to play. Get Minetest at <https://minetest.net>.

## How to play

Your trusty companion, the Light Crystal, is guiding you through the game.
To attack the Shadows, use the Staff of Light by pressing the [Punch] key
(default: left click).

Collect resources by walking over them.
Hint: Break vases and use the campfire!

## Credits

With the exception of translations, the entire game
(concept, gameplay code, graphics, sounds, music, story etc.)
was created by Wuzzy.

Translation credits:
* German: Wuzzy
* Spanish: José M

### Mods

All mods were created by Wuzzy during the 2023 Minetest Game Jam, with
one exception:

* `playerphysics`: Minimalist API for easy player physics handling
                   (an old mod by Wuzzy)

## License

Shadow Forest is free software, made by Wuzzy. Copyright (C) 2023.

License summary:

* Code: GPLv3 or later
* Media files and everything else: CC BY-SA 4.0

See `LICENSE.txt` for details.

### Software license

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

### Media license

The media files and every other non-code-related file is licensed
under:

Creative Commons Attribution-ShareAlike 4.0 International
<https://creativecommons.org/licenses/by-sa/4.0>.
